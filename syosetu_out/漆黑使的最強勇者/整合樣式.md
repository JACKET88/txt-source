---
LocalesID: 漆黑使的最強勇者
LocalesURL: https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E6%BC%86%E9%BB%91%E4%BD%BF%E7%9A%84%E6%9C%80%E5%BC%B7%E5%8B%87%E8%80%85.ts
---
__TOC__

[漆黑使的最強勇者](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E6%BC%86%E9%BB%91%E4%BD%BF%E7%9A%84%E6%9C%80%E5%BC%B7%E5%8B%87%E8%80%85.ts)  
總數：24／25

# Pattern

## 西恩

### patterns

- `シオン`

## 阿克斯

### patterns

- `アックス`

## 哈庫

### patterns

- `ハク`

## 哈庫珂

### patterns

- `ハクコ`

## 卡魯

### patterns

- `カルー`
- `カール`

## 琳

### patterns

- `リン`
- `玲`

## 瑪麗

### patterns

- `マリー`

## 波魯加

### patterns

- `オルガン`

## 黃金頭盔

### patterns

- `ゴールドヘルム`

## 多魯

### patterns

- `ドル`

## 加魯

### patterns

- `ガル`

## 尤娜

### patterns

- `ユーナ`

## 菲魯姆

### patterns

- `フェルム`

## 羅尼

### patterns

- `ローネ`

## 薩布朗

### patterns

- `サブラン`

## 沙之駱

### patterns

- `サツロー`

## 丈

### patterns

- `ジャン`

## 艾巴登

### patterns

- `エドバン`
- `愛德班`

## 蘭迪魯

### patterns

- `ランディル`

## 莉莉

### patterns

- `リリー`

## 雙重雷光

### patterns

- `ダブルライトニング`

## 三頭蛇

### patterns

- `三つ蛇`

## 阿魯多之劍

### patterns

- `アルトセイバー`

## 義賣會

### patterns

- `バザール`


