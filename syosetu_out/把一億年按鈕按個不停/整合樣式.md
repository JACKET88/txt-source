---
LocalesID: 把一億年按鈕按個不停
LocalesURL: https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E6%8A%8A%E4%B8%80%E5%84%84%E5%B9%B4%E6%8C%89%E9%88%95%E6%8C%89%E5%80%8B%E4%B8%8D%E5%81%9C.ts
---
__TOC__

[把一億年按鈕按個不停](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E6%8A%8A%E4%B8%80%E5%84%84%E5%B9%B4%E6%8C%89%E9%88%95%E6%8C%89%E5%80%8B%E4%B8%8D%E5%81%9C.ts)  
總數：23／23

# Pattern

## 亞蓮

### patterns

- `アレン`

## 羅德爾

### patterns

- `ロードル`
- `洛德爾`

## 亞蓮＝羅德爾

### patterns

- `亞蓮#_@_#羅德爾`

## 多多利艾爾

### patterns

- `ドドリエル`

## 波頓

### patterns

- `バートン`

## 寶菈

### patterns

- `ポーラ`
- `寶拉`

## 加雷德贊爾

### patterns

- `ガレッドザール`

## 格蘭

### patterns

- `グラン`
- `古蘭`

## 蕾雅

### patterns

- `レイア`

## 拉斯諾特

### patterns

- `ラスノート`

## 蘿絲

### patterns

- `ローズ`
- `夢絲`

## 巴倫西亞

### patterns

- `バレンシア`
- `羅巴崙西亞`

## 蘿絲＝巴倫西亞

### patterns

- `蘿絲#_@_#巴倫西亞`

## 莉亞

### patterns

- `リア`

## 維斯特利亞

### patterns

- `ヴェステリア`

## 莉亞＝維斯特利亞

### patterns

- `莉亞#_@_#維斯特利亞`

## 菲利斯

### patterns

- `フェリス`

## 多拉海茵

### patterns

- `ドーラハイン`

## 巴布爾

### patterns

- `バブル`

## 戈薩

### patterns

- `ゴザ`

## 丹莉亞

### patterns

- `ダリア`

## 歐勒斯特

### patterns

- `オーレスト`
- `歐利斯特`

## YAIBA

### patterns

- `ヤイバ`


