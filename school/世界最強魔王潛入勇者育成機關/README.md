# novel

- title: 世界最強の魔王ですが誰も討伐しにきてくれないので、勇者育成機関に潜入することにしました。
- title_zh: 世界最強魔王潛入勇者育成機關
- author: 両道 渡
- illust: azuタロウ
- source: http://ncode.syosetu.com/n3995ex/
- cover: https://images-na.ssl-images-amazon.com/images/I/91SegQ2KZAL.jpg
- publisher: syosetu
- date: 2019-10-26T07:00:00+08:00
- status: 連載
- novel_status: 0x0100

## illusts


## publishers

- syosetu

## series

- name: 世界最強の魔王ですが誰も討伐しにきてくれないので、勇者育成機関に潜入することにしました。

## preface


```
世界最強的魔王路西法憑藉其壓倒性的力量支配著魔族。
最愛的妻子在內的後宮也開了，只有吃飯、幹活、睡覺的生活持續了數百年以上。
可是沉眠於魔王血中的破壞衝動尋求強敵開始作痛了。
盡管等了這麼久可為什麼勇者不來討伐自己呢，實在是豈有此理！
那樣想的路西法決定化為人類，去看看作為勇者育成機關的軍校的情況。
要知道內部的情況入學是最好的。雖然路西法輕易突破了入學考試平安入學了但是……！？
這是最強的魔王不知搞錯了什麼而成為至高的勇者的故事。

☆書籍第2巻が10月21日発売です！！　また、ドラマCDの発売も決定致しました！
＊書籍第1巻がガガガブックスさまより発売中です。


世界最強の魔王ルシファーはその圧倒的な力で魔族を支配していた。
最愛の妻を交えたハーレムも作って、食う、ヤる、寝るだけの生活が数百年以上続く。
しかし魔王の血の中に眠る破壊衝動が強敵を求めて疼き出してしまった。
これだけ待ってるにもかかわらず何故勇者は自分を討伐しに来ないのか、実にけしからん！
そう思ったルシファーは人間に化けて、勇者育成機関である軍学校の様子を見に行くことにした。
内部の様子を知るには入学するのが一番。入学試験をあっさり突破して無事に入学したルシファーだったが……！？
これは最強の魔王が何を間違ったのか至高の勇者となる物語である。
```

## tags

- node-novel
- R15
- syosetu
- エルフ
- ハイファンタジー
- ハイファンタジー〔ファンタジー〕
- ハーレム
- ファンタジー
- メイド妻
- 主人公最強
- 剣と魔法
- 勇者
- 天使妻
- 妻帯者
- 学園
- 弱体化しても圧倒的
- 最強過ぎるので弱体化
- 残酷な描写あり
- 異世界
- 陰謀
- 魔族
- 魔王

# contribute

- 土之女神

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 1

## syosetu

- txtdownload_id:
- series_id:
- novel_id: n3995ex

## textlayout

- allow_lf2: true

# link

- [narou.nar.jp](https://narou.nar.jp/search.php?text=n3995ex&novel=all&genre=all&new_genre=all&length=0&down=0&up=100) - 小説家になろう　更新情報検索
- [小説情報](https://ncode.syosetu.com/novelview/infotop/ncode/n3995ex/)
- https://masiro.moe/forum.php?mod=forumdisplay&fid=260&page=1
- [梗角色转生吧](https://tieba.baidu.com/f?kw=%E6%A2%97%E8%A7%92%E8%89%B2%E8%BD%AC%E7%94%9F&ie=utf-8&tp=0 "")
-

