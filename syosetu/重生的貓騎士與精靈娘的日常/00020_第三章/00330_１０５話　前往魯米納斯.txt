「嗯喵~！坐船旅行真是久違了喵！」

第二天——薇兒卡在船上吹著海風伸展身體。

艾麗婭一行人來到迷宮都市郊區的港口，做好了乘船的準備後便出發了。
目的地當然就是瓦沙克的現身之地——精靈之里魯米納斯了。
魯米納斯在一個名為澤哈德的國家裡，翻過山脈便能到達。不過現在這個時期，順著洋流走水路的話會更快一些。


「我還是第一次看到海哇！」
「一片汪洋~！」
「似有奇特的味道存在吶！」

第一次看到大海的莉莉和菲莉興奮不已，史黛拉則因為海水中帶有的鹽味而興緻勃勃。


「朱利烏斯殿下，您的身體還好嗎……？」


在甲板的一角，朱利烏斯皇子正在閉目養神，任憑海風吹拂，因此艾麗婭擔憂地上前問道。


和薇兒卡一樣，朱利烏斯皇子也決定參戰，不過他的神色並不太好，上一次的戰鬥的疲勞還沒完全消除的樣子。


「並無大礙，艾麗婭。大鬧一通……估計是不太可能了，不過在後方指揮部隊之類的尚無妨」


對艾麗婭的擔憂，朱利烏斯皇子輕輕點了點頭以回應。


艾麗婭她們曾不著痕跡地表示她們有著鍊金葯，並想用那個幫朱利烏斯皇子治療身體……可是朱利烏斯皇子的病癥，是常年過度使用勇者之力造成的，嚴重到連鍊金葯都無能為力的地步了。


順帶一提，這艘船上搭乘的人員有艾麗婭一行人、艾莉莎和朱利烏斯皇子……。
除此之外還有以丹尼為首的騎士們和十幾名冒險者在其中。


騎士們是作為任務參戰，而冒險者們則是自願參加這次戰役。


瓦沙克在迷宮裡召喚出了召喚獸，而打敗了它們的正是艾麗婭她們……。要是沒有她們，說不定迷宮都市現在已經淪陷了。
而艾麗婭的故鄉正面臨危機……一聽到這個消息的高等冒險者們紛紛「這次輪到我們去助艾麗婭她們一臂之力了！」地申請參戰。

「雖然我已經引退了，不過我的實力可還沒有消退哦♪」


在艾麗婭一旁的是公會的櫃檯妹……阿納爾德・霍茲尼格爾。
她原本就是B級冒險者，本著多一分力量就多一分勝機的想法，她也決定以個人名義參加到這一次的戰役中。

(對面是四魔族的瓦沙克率領的大軍……不過這邊也不僅僅只有這一點戰力，還能得到魯米納斯的戰士們的協力，而且塞德里克殿下也會在那裡匯合，所以不會這麼簡單就被打敗的)


被艾麗婭抱在懷中的塔瑪在腦海裏再次確認目前的狀況。


由於敵方是要用大軍來進攻，那這邊也要湊齊對應的人數來對抗。
根據艾莉莎的情報來看，等到瓦沙克的力量恢復完全，之後軍隊一旦整頓完畢，他就會使用轉移系技能將大軍一口氣傳送過來發動進攻。
魯米納斯有著賢者之石的結界在，是不可能直接傳送到村子裡去的……因此這邊預測他會把軍隊轉移到村子附近的郊區那裡去。


「吶吶，艾麗婭，我能久違地抱一抱塔瑪醬嗎？」
「我也想再抱一抱呢」

艾麗婭抱著塔瑪站在甲板上，一臉緊張地眺望著遠方，而騎士隊的二人……凱妮和瑪麗埃塔則向著那樣的她搭話了。
單純想要抱抱塔瑪的想法確實是有的，不過……除此之外，她們還想借此來驅散艾麗婭那緊張的情緒吧。


「當然可以！塔瑪，能讓凱妮和瑪麗埃塔也抱一抱你嗎？」
「喵~啊(好的，主人)！」


對此，塔瑪精神地回應。
被主人以外的女性抱抱，對騎士來說有點不盡喵意，但如果是主人的命令的話就沒辦法了。
於是塔瑪便被兩位穿著比基尼鎧甲的女騎士夾在胸中進行抱抱活動。


「果然毛茸茸最贊了吶~！」
「就是！這順滑的毛毛擼起來真的很舒服啊！」


凱妮將塔瑪抱在胸裡，瑪麗埃特則撫摸著他的身體。
寫作貝希摩斯讀作幼貓的塔瑪在那溫柔鄉中舒服得骨軟筋酥，貓眼迷濛。


「啊！狡猾！吾亦要抱塔瑪！」
「我也想摸摸塔瑪哇！」
「我也要~！」


看到凱妮和瑪麗埃特在寵愛著塔瑪，史黛拉、莉莉和菲莉都紛紛說要摻一腳。


將史黛拉視為情敵的艾麗婭自然不會坐視不管，唯獨阻止了史黛拉對塔瑪的抱抱……然而，一旦二人開始互相爭奪，塔瑪就一如既往地被夾在艾麗婭和史黛拉的雙重蜜瓜中間，變成奶貓三文治了。


(唔喵~……變身成聖獸的塔瑪醬，好帥啊喵……)


望著那個雙奶戲貓的光景，薇兒卡心裡卻想著這事。
那頭威風凜凜的純白色獅子——明明那時還在戰鬥途中，她卻被那個身姿奪去了思緒。


艾麗婭她們曾抱著一試的想法把塔瑪變身的事告訴了艾莉莎，想要聽一下有沒有相關的情報，而得到的回答是她也不太清楚。


(呵呵……塔瑪醬，比之前又強上一分了呢)


看著在艾麗婭和史黛拉兩人豐滿的蜜瓜中「嗚喵~~~~！？」地掙扎的塔瑪，艾莉莎的臉上露出了微笑。